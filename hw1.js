//1.Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
//(1)Прототипне наслідування в JavaScript - це спосіб створення об'єктів, який дозволяє об'єктам наслідувати властивості та методи інших об'єктів, що називаються прототипами.


//2.Для чого потрібно викликати super() у конструкторі класу-нащадка?
//(2)Виклик super() у конструкторі класу - нащадка в JavaScript є необхідним для виклику конструктора класу - батька.Це дозволяє класу - нащадку успадкувати функціональність і властивості від свого батьківського класу та проводити необхідну ініціалізацію.
// Коли клас - нащадок має свій власний конструктор, виклик super() допомагає забезпечити правильне наслідування та ініціалізацію.Виклик super() може передавати необхідні аргументи батьківському конструктору, якщо потрібно, або просто викликати конструктор батьківського класу без аргументів, якщо це потрібно.


// Клас Employee
class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }

  // Гетери та сеттери
  get getName() {
    return this.name;
  }

  set setName(name) {
    this.name = name;
  }

  get getAge() {
    return this.age;
  }

  set setAge(age) {
    this.age = age;
  }

  get getSalary() {
    return this.salary;
  }

  set setSalary(salary) {
    this.salary = salary;
  }
}

// Клас Programmer, що успадковує клас Employee
class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }

  // Перезапис гетера для властивості salary
  get getSalary() {
    return this.salary * 3;
  }
}

// Створення екземплярів класу Programmer
const programmer1 = new Programmer('Yaroslav', 30, 50000, ['C++', 'Python']);
const programmer2 = new Programmer('Artem', 25, 60000, ['JS', 'C#']);

// Виведення інформації про програмістів у консоль
console.log("Programmer 1:", programmer1.getName, programmer1.getAge, programmer1.getSalary, programmer1.lang);
console.log("Programmer 2:", programmer2.getName, programmer2.getAge, programmer2.getSalary, programmer2.lang);